

## Information

- Change ownerNumber in tobz.js to be your number
ownerNumber = "6213xxxxxx@c.us"
- Change adminNumber in admin.json to be your number
[] -> ["62813xxxxxx@c.us"]

## Contact

If you find some bugs please contact the WhatsApp number on Contact

- [Telegram](https://t.me/tobz2k19)
- [Whatsapp](https://wa.me/6281311850715)
- [BOT WA](wa.me/447723992261)
- [GROUP WA](https://chat.whatsapp.com/InZrGpgBBNB4hnS7Q6sVJG)

## APIKEY
Open tobz.js then edit & paste it in YOUR_APIKEY
- [VHTEAR](https://api.vhtear.com)
- [MHANKBARBAR](https://mhankbarbar.herokuapp.com/api)

## Donate
- [Saweria](https://saweria.co/ItzTobz)
- [Dana](https://link.dana.id/qr/17mlqta)

## Getting Started

### This project require NodeJS v14.
This is a Bot Group Type, so you have to enter the bot into a group in order to fully use the command!

### Install
Clone this project

```bash
> git clone https://github.com/TobyG74/ElainaBOT
> cd ElainaBOT
```

Install the dependencies:

```bash
> npm install
```

### Usage
1. run the Whatsapp bot

```bash
> npm start
```

after running it you need to scan the qr



| Sticker Creator |                Feature           |
| :-----------: | :--------------------------------: |
|       ✅       | Sticker With Image          |
|       ✅       | Sticker With Gif                    |
|       ✅       | Sticker With Image Url                        |
|       ✅       | Sticker With Gif Url   |
|       ✅       | Image To Sticker   |
|       ✅       | Text To Picture   |

| Downloader |                     Feature                |
| :------------: | :---------------------------------------------: |
|       ✅        |   YouTube Video/Audio Downloader                    |
|       ✅        |   Doujin Downloader         |
|       ✅        |   Instagram Video/Image Downloader                  |
|       ✅        |   Facebook Video Downloader                  |
|       ✅        |  Tiktok Downloader                    |
|       ✅        |   Twitter Downloader         |
|       ✅        |   Smule Mp3 Downloader                  |
|       ✅        |   Starmaker Video Downloader                  |
|       ✅        |   Xnxx Video Downloader                  |
|       ✅        |  Joox Downloader                  |

| Kerang Ajaib  |                     Feature                     |
| :------------: | :---------------------------------------------: |
|       ✅        |   Apakah             |
|       ✅        |   Bisakah                |
|       ✅        |   Rate             |
|       ✅        |   Kapankah           |

| Media  |                     Feature                     |
| :------------: | :---------------------------------------------: |
|       ✅        |   Get a random meme             |
|       ✅        |   Text to speech                |
|       ✅        |   Get a random waifu images     |
|       ✅        |   Get a random quotes           |
|       ✅        |   Get a random anime quotes     |
|       ✅        |   Get info gempa from BMKG      |
|       ✅        |   Weather's report's     |
|       ✅        |   Wikipedia                 |
|       ✅        |   Youtube                 |
|       ✅        |   Google                 |
|       ✅        |   Pinterest    |
|       ✅        |   Anime searcher    |
|       ✅        |   Google Image               |
|       ✅        |   Couple Fortune Telling    |
|       ✅        |   Chord    |
|       ✅        |   Prayer Times    |
|       ✅        |   Lyrics    |
|       ✅        |   Textmaker    |
|       ✅        |   Instagram Stalk    |
|       ✅        |   Tiktok Stalk    |
|       ✅        |   Smule Stalk    |
|       ✅        |   Write in Image    |
|       ✅        |   Weather Information    |
|       ✅        |   Get a random cat images       |
|       ✅        |   Get a random dog images       |
|      And        |   Others...                     |


| Group Only  |                     Feature                     |
| :------------: | :---------------------------------------------: |
|       ✅        |   Promote User                  |
|       ✅        |   Demote User                   |
|       ✅        |   Kick User                     |
|       ✅        |   Add User                      |
|       ✅        |   Mention All User              |
|       ✅        |   Get link group                |
|       ✅        |   Get Admin list                |
|       ✅        |   Listblock                      |
|       ✅        |   Listbanned                      |
|       ✅        |   Listgroup                      |
|       ✅        |   Get Elaina Admin list           |
|       ✅        |   Get owner group               |
|       ✅        |   Get group info                |
|       ✅        |   enable or disable nsfw command|
|       ✅        |   enable or disable simi command|
|       ✅        |   enable or disable welcome feature|
|       ✅        |   enable or disable left feature|

| Admin Bot Only  |              Feature                |
| :------------: | :---------------------------------------------: |
|       ✅        |   Banned                 |
|       ✅        |   Unbanned                 |
|       ✅        |   Mute                 |
|       ✅        |   Unmute                 |

| Owner Group Only  |              Feature                |
| :------------: | :---------------------------------------------: |
|       ✅        |   Kick All Member Group                 |

| Owner Bot Only  |              Feature                |
| :------------: | :---------------------------------------------: |
|       ✅        |   Leave all group                   |
|       ✅        |   Clear all message                 |
|       ✅        |   Broadcast                      |
|       ✅        |   Getses                      |
|       ✅        |   Banchat                      |
|       ✅        |   Maintenance                      |
|       ✅        |   Add Admin Elaina                      |
|       ✅        |   Del Admin Elaina                      |
|       ✅        |   Block                      |
|       ✅        |   Unblock                      |
|       ✅        |   Join Group                      |

---

## Troubleshooting
Make sure all the necessary dependencies are installed: https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md

Fix Stuck on linux, install google chrome stable: 
```bash
> wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
> sudo apt install ./google-chrome-stable_current_amd64.deb
```

## 🙏Special Thanks To
<ul>
<li>https://github.com/open-wa/wa-automate-nodejs<br>
<li>https://github.com/YogaSakti/imageToSticker<br>
<li>https://github.com/MhankBarBar/whatsapp-bot<br>
<li>https://github.com/ItzNgga/wa-bot<br>
</li>
